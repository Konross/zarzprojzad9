public class PrimesSummary 
{
	public static void main(String[] args) {

		long sum = 2;
		for (int i=3; i<2000000; i++) {
			if(isPrime(i)) {
				sum = sum + i;
			}    
		}
		System.out.println("Suma liczb pierwszych ponizej 2mln  = "+sum); 
	}
	
	 static boolean isPrime(int n) {
		if (n < 2) {
			return false;
		}
		if (n == 2 || n == 3) {
			return true;
		}
		if ((n & 1) == 0 || n % 3 == 0) {
			return false;
		}
		int Sqrt = (int) Math.sqrt(n) + 1;
		//sprawdzamy czy liczba jest liczba pierwsza
		//jesli nie to zwracamy false, jesli tak to true
		//if(checkSqrt(Sqrt)==false){
		//	return false;
		//}
			
		//return true;
		boolean check = checkSqrt(Sqrt,n);
		return check;
	}
	
	static boolean checkSqrt(int sqrtN, int k){
		for (int i = 6; i <= sqrtN; i += 6) {
			if (k % (i - 1) == 0 || k % (i + 1) == 0) {
				return false;
			}
		}
		return true;
	}
}